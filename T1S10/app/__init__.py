class Calculadora (object):
    def __init__(self, num1, num2):
        self.num1 = num1
        self.num2 = num2

    def suma(self):
        if self.num2 < 0:
            return "La Suma de {}-{} ({}+{}) es igual a {}".format(self.num1, -self.num2, self.num1, self.num2, self.num1 + self.num2)
        else:
            return "La Suma de {}+{} es igual a {}".format(self.num1, self.num2, self.num1 + self.num2)

    def resta(self):
        if self.num2 < 0:
            return "La Resta de {}+{} ({}-{}) es igual a {}".format(self.num1, -self.num2, self.num1, self.num2, self.num1 - self.num2)
        else:
            return "La Resta de {}-{} es igual a {}".format(self.num1, self.num2, self.num1 - self.num2)

    def multiplicacion(self):
        if (self.num1 * self.num2 == -0.0) and (self.num1 < 0 or self.num2 < 0):
            return "La Multiplicacion de {}*{} es igual a {}".format(self.num1, self.num2, -(self.num1 * self.num2))
        else:
            return "La Multiplicacion de {}*{} es igual a {}".format(self.num1, self.num2, self.num1 * self.num2)

    def division(self):
        if (self.num1 / self.num2 == -0.0) and (self.num1 < 0 or self.num2 < 0):
            return "La Division de {}/{} es igual a {}".format(self.num1, self.num2, -(self.num1 / self.num2))
        elif self.num2 == 0:
            return "No se pueden realizar divisiones entre cero ({}/{})".format(self.num1, self.num2)
        elif self.num1 == 0 and  self.num2 < 0:
            self.num2 = int(self.num2)
            return "La Division de {}/{} es igual a {}".format(self.num1, self.num2, -(self.num1 / self.num2))
        else:
            return "La Division de {}/{} es igual a {}".format(self.num1, self.num2, self.num1/self.num2)

    def potencia(self):
        if self.num2 < 0:
            return "El resultado de la potencia negativa {}^{} es  1/{} o {}".format(self.num1, self.num2, self.num1**-self.num2, self.num1**self.num2)
        elif self.num2 == 0:
            return "Todo numero elevado a la 0 es igual a 1 ({}^{})".format(self.num1, self.num2)
        else:
            return "La Potencia de {}^{} es igual a {}".format(self.num1, self.num2, self.num1**self.num2)

    def raiz(self):
        if self.num1 < 0 and self.num2 % 2 == 0:
            return "No se pueden realizar raices pares de numeros negativos ({}√{})".format(self.num2, self.num1)
        elif self.num1 < 0 and self.num2 % 2 != 0:
            self.num1 = -self.num1
            self.num1 = int(self.num1)
            return "La Raiz impar ({}) del numero negativo {} ({}√{}) es igual a {}".format(self.num2, -self.num1, self.num2, -self.num1, -(self.num1 ** (1 / self.num2)))
        elif self.num2 == 0:
            return "No se pueden realizar raices a la 0 ({}√{})".format(self.num2, self.num1)
        elif self.num2 < 0:
            self.num2 = -self.num2
            self.num2 = int(self.num2)
            return "La Raiz negativa de {}√{} es igual a 1/{} o {}".format(-self.num2, self.num1, self.num1**(1/self.num2), 1/(self.num1**(1/self.num2)))
        else:
            return "La Raiz de {}√{} es igual a {}".format(self.num2, self.num1, self.num1**(1/self.num2))


if __name__ == "__main__":
    while True:
        print("Calculadora que realiza operaciones entre dos numeros reales\n")
        num1 = float(input("Ingrese el primer numero: "))
        num2 = float(input("Ingrese el segundo numero: "))
        c = Calculadora(num1, num2)
        print("---Menu Principal---")
        print("1. Suma\n2. Resta\n3. Multiplicacion\n4. Division\n5. Potencia\n6. Raiz\n7. Salir")
        opcion = int(input("Seleccione: "))
        if opcion == 7:
            break
        elif opcion == 1:
            print(c.suma())
        elif opcion == 2:
            print(c.resta())
        elif opcion == 3:
            print(c.multiplicacion())
        elif opcion == 4:
            print(c.division())
        elif opcion == 5:
            print(c.potencia())
        elif opcion == 6:
            print(c.raiz())
        elif opcion == 7:
            break
        else:
            print("Opcion Invalida, Intente nuevamente")
        while True:
            continuar = input("Desea continuar? (S/N): ")
            if continuar.lower() == "n" or continuar.lower() == "no":
                exit()
            elif continuar.lower() == "s" or continuar.lower() == "si":
                break
            else:
                print("Seleccion Invalida, Intente nuevamente")
        print("\n\n")
